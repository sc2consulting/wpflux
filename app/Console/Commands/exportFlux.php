<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class exportFlux extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:flux';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $outputFile = "resultp.json";
        $fichier_destination = public_path() .'/json/'. $outputFile;
        $array2 = array();
        // $wp_article = \DB::table('wp_term_relationships')->distinct('object_id')->get();
        $wp_article = \DB::table('wp_term_relationships')
        ->select('object_id')
        ->groupBy('object_id')
        ->orderBy('object_id','desc')
        ->get();


        unlink($fichier_destination);
        $fp = fopen($fichier_destination,"a+");
              fwrite($fp, '');
              fclose($fp);

        $total = 0;

        foreach ($wp_article as $k => $wp) {
          // $jsonapi = file_get_contents('https://lasanteauquotidien.com/wp-json/wp/v2/posts/'.$wp->object_id);
          $r = $this->get_web_page('https://lasanteauquotidien.com/wp-json/wp/v2/posts/'.$wp->object_id);
            $data = json_decode($r['content']);
            if (!empty($data)) {
              if(isset($data->id)){
                var_dump($data->id);
                $j = $this->get_web_page('https://lasanteauquotidien.com/wp-json/wp/v2/media/'.$data->featured_media);
                $data2 = json_decode($j['content']);

                if(isset($data2->guid->rendered)){
                $array = array(
                    'id' => $data->id,
                    'link' => $data->link,
                    'title' => $data->title->rendered,
                    'date' => $data->date,
                    'content_rendered' => $data->content->rendered,
                    'excerpt_rendered' => $data->excerpt->rendered,
                    'img_link' => $data2->guid->rendered,
                );

                $total = $total + 1;
                array_push($array2, $array);

                }

        }

    }
  }

  $chaine = json_encode($array2, JSON_PRETTY_PRINT);

  $fp = fopen($fichier_destination,"w+");
        fwrite($fp, $chaine);
        fclose($fp);
        unset($chaine);
        echo 'In JSON' . "\n";
        $total = 0;
        $array2 = array();

}
    function get_web_page( $url )
    {
        $user_agent='Mozilla/5.0 (Windows NT 6.1; rv:8.0) Gecko/20100101 Firefox/8.0';

        $options = array(

            CURLOPT_CUSTOMREQUEST  =>"GET",        //set request type post or get
            CURLOPT_POST           =>false,        //set to GET
            CURLOPT_USERAGENT      => $user_agent, //set user agent
            CURLOPT_COOKIEFILE     =>"cookie.txt", //set cookie file
            CURLOPT_COOKIEJAR      =>"cookie.txt", //set cookie jar
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => false,    // don't return headers
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
        );

        $ch      = curl_init( $url );
        curl_setopt_array( $ch, $options );
        $content = curl_exec( $ch );
        $err     = curl_errno( $ch );
        $errmsg  = curl_error( $ch );
        $header  = curl_getinfo( $ch );
        curl_close( $ch );

        $header['errno']   = $err;
        $header['errmsg']  = $errmsg;
        $header['content'] = $content;
        return $header;
    }


}
